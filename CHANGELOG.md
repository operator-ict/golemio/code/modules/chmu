# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.8] - 2024-11-28

### Added

-   AsyncAPI documentation ([integration-engine#263](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/263))

## [1.0.7] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.0.6] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.0.5] - 2023-03-27

### Fixed

-   Add missing `SQL_DUMP_FILES` to `.env.template` (needed for tests to work)

## [1.0.4] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.0.3] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.0.2] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.0.1] - 2023-02-23

### Changed

-   `measuredAt` in table `measurements` changed from timestamp without zone to timestampz

## [1.0.0] - 2023-02-20

### Added

-   Integration of chmu measurements

### Changed

-   Migrate to npm

