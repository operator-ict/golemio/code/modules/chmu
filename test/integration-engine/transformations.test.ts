import { MeasurementsTransformation } from "#ie/transformations/MeasurementsTransformation";
import IInputChmuData from "#sch/datasources/interfaces/IInputChmuData";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

describe("MeasurementsTransformation", () => {
    let transformation: MeasurementsTransformation;
    let sourceData: IInputChmuData[];

    before(() => {
        transformation = new MeasurementsTransformation();
        sourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/6443_2023021312.json").toString("utf8"));
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("MeasurementsTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(sourceData);
        expect(data[0]).to.deep.equal({
            measured_at: "2023-02-13T12:00:00.000+01:00",
            station_id: "C1ROZM01",
            year: 2023,
            month: 2,
            day: 13,
            time: "12:00",
            air_temp: 5.6,
            air_hum: 67,
            precip: 0,
            air_pressure: null,
            wind_speed: 0.5,
            wind_dir: 257,
            sunlight: 0,
            soil_hum_0_10: null,
            soil_hum_10_50: null,
            soil_hum_50_100: null,
        });
    });
});
