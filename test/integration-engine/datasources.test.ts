import ChmuDatasource from "#ie/datasources/ChmuDatasource";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("ChmuDatasource", () => {
    let datasource: ChmuDatasource;

    before(() => {
        datasource = new ChmuDatasource({} as any);
    });

    it("should have name", async () => {
        expect(datasource.name).not.to.be.undefined;
        expect(datasource.name).is.equal("ChmuDatasource");
    });

    it("should generate correct filename", async () => {
        expect(datasource["generateFileName"](new Date(2023, 1, 13, 12))).equal("6443_2023021312.txt");
    });
});
