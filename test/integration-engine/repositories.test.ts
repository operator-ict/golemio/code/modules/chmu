import { MeasurementsRepository } from "#ie/data-access/MeasurementsRepository";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("MeasurementsRepository", () => {
    let repo: MeasurementsRepository;

    before(() => {
        PostgresConnector.connect();
        repo = new MeasurementsRepository();
    });

    it("should have name", async () => {
        expect(repo.name).not.to.be.undefined;
        expect(repo.name).is.equal("ChmuMeasurementsRepository");
    });

    it("should have updateActive method", async () => {
        expect(repo.bulkUpdate).not.to.be.undefined;
    });
});
