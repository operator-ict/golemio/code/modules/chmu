import { ChmuContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import IDownloadMeasurementsMessage from "#ie/workers/interfaces/IDownloadMeasurementsMessage";
import DownloadMeasurementsTask from "#ie/workers/tasks/DownloadMeasurementsTask";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("DownloadMeasurementsTask", () => {
    let sandbox: SinonSandbox;
    let task: DownloadMeasurementsTask;
    let getDataStub: SinonStub, transformStub: SinonSpy, bulkUpdateStub: SinonSpy;

    before(() => {
        sandbox = sinon.createSandbox();
        task = ChmuContainer.resolve<DownloadMeasurementsTask>(ModuleContainerToken.DownloadMeasurementsTask);
    });

    it("should have queueName", async () => {
        expect(task.queueName).not.to.be.undefined;
        expect(task.queueName).is.equal("downloadMeasurementsTask");
    });

    it("should download data and save to db", async () => {
        getDataStub = sandbox.stub(task["datasource"], "getData").callsFake((data) => {
            return {
                data: JSON.parse(fs.readFileSync(__dirname + "/../data/6443_2023021312.json").toString("utf8")),
            } as any;
        });
        transformStub = sandbox.spy(task["transformation"], "transform");
        bulkUpdateStub = sandbox.spy(task["repository"], "bulkUpdate");

        await expect(task["execute"]({} as IDownloadMeasurementsMessage)).to.be.fulfilled;
        expect(getDataStub.callCount).equal(1);
        expect(transformStub.callCount).equal(1);
        expect(bulkUpdateStub.callCount).equal(1);
    });
});
