# Implementační dokumentace modulu *Chmu*

## Záměr

Modul slouží ke stažení chmu dat z ftp agregovaných po hodině do csv souborů.

## Vstupní data

### Data aktivně stahujeme

Popis dat, jak je pomocí cronu a integration-engine stahujeme. Pokud jsou data stahována z více zdrojů, každý zdroj je popsán zvlášť.

#### *ChmuDatasource*

- zdroj dat
  - golemio ftp
- formát dat
  - csv soubor
  - [validační schéma: ](https://gitlab.com/operator-ict/golemio/code/modules/chmu/-/blob/development/src/schema-definitions/datasources/ChmuSchemaProvider.ts)
  - [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/chmu/-/blob/development/test/data/6443_2023020215.txt)
- frekvence stahování
  - jednou za hodinu
- název rabbitmq fronty
  - `chmu.downloadMeasurementsTask`

## Zpracování dat / transformace

Popis transformace a případného obohacení dat. Zaměřeno hlavně na workery a jejich metody.

### *ChmuWorker*

Stručný popis workeru.

#### *DownloadMeasurementsTask*

- vstupní rabbitmq fronta
  - název `chmu.downloadMeasurementsTask`
  - parametry `from` and `to` volitelné parametry v iso formátu
- datové zdroje
  - `ChmuDatasource`
- transformace
  - odkaz na transformaci dat, případně stručný popis tranformace
  - [transformace dat](https://gitlab.com/operator-ict/golemio/code/modules/chmu/-/blob/development/src/integration-engine/transformations/MeasurementsTransformation.ts)
- data modely
  - [MeasurementModel](https://gitlab.com/operator-ict/golemio/code/modules/chmu/-/blob/development/src/schema-definitions/definitions/models/MeasurementModel.ts)



## Uložení dat

Popis ukládání dat.

### Obecné

- typ databáze
  - PSQL
- datábázové schéma
  - schéma: `chmu`
  ![chmu er diagram](assets/chmu.erd.png)
- retence dat
  - data jsou zachována
- AsyncApi dokumentace
  - [AsyncApi](./asyncapi.yaml)
