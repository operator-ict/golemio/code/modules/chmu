ALTER TABLE measurements ALTER COLUMN air_hum TYPE varchar USING air_hum::varchar;
ALTER TABLE measurements ALTER COLUMN air_temp TYPE varchar USING air_temp::varchar;
ALTER TABLE measurements ALTER COLUMN precip TYPE varchar USING precip::varchar;
ALTER TABLE measurements ALTER COLUMN air_pressure TYPE varchar USING air_pressure::varchar;
ALTER TABLE measurements ALTER COLUMN wind_speed TYPE varchar USING wind_speed::varchar;
ALTER TABLE measurements ALTER COLUMN wind_dir TYPE varchar USING wind_dir::varchar;
ALTER TABLE measurements ALTER COLUMN sunlight TYPE varchar USING sunlight::varchar;