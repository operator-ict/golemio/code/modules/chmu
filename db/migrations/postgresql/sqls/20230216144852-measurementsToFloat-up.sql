ALTER TABLE measurements ALTER COLUMN air_hum TYPE float8 USING air_hum::float8;
ALTER TABLE measurements ALTER COLUMN air_temp TYPE float8 USING air_temp::float8;
ALTER TABLE measurements ALTER COLUMN precip TYPE float8 USING precip::float8;
ALTER TABLE measurements ALTER COLUMN air_pressure TYPE float8 USING air_pressure::float8;
ALTER TABLE measurements ALTER COLUMN wind_speed TYPE float8 USING wind_speed::float8;
ALTER TABLE measurements ALTER COLUMN wind_dir TYPE float8 USING wind_dir::float8;
ALTER TABLE measurements ALTER COLUMN sunlight TYPE float8 USING sunlight::float8;
