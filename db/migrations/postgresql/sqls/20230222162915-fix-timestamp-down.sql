ALTER TABLE measurements ALTER COLUMN measured_at TYPE timestamp without time zone USING measured_at::timestamp without time zone;
