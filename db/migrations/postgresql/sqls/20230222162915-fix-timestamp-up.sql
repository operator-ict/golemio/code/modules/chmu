ALTER TABLE measurements ALTER COLUMN measured_at TYPE timestamptz USING measured_at::timestamptz;
