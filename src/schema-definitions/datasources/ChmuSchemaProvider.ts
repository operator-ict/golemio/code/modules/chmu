export default class ChmuSchemaProvider {
    public static get() {
        return {
            type: "array",
            items: {
                type: "object",
                required: [
                    "YEAR",
                    "MONTH",
                    "DAY",
                    "TIME",
                    "EG_GH_ID",
                    "T",
                    "H",
                    "SRA1H",
                    "P",
                    "F",
                    "D",
                    "SSV1H",
                    "HPU1",
                    "HPU2",
                    "HPU3",
                ],
                additionalProperties: true,
                properties: {
                    YEAR: { type: "string" },
                    MONTH: { type: "string" },
                    DAY: { type: "string" },
                    TIME: { type: "string" },
                    EG_GH_ID: { type: "string" },
                    T: { type: "string" },
                    H: { type: "string" },
                    SRA1H: { type: "string" },
                    P: { type: "string" },
                    F: { type: "string" },
                    D: { type: "string" },
                    SSV1H: { type: "string" },
                    HPU1: { type: "string" },
                    HPU2: { type: "string" },
                    HPU3: { type: "string" },
                },
            },
        };
    }
}
