export default interface IInputChmuData {
    YEAR: string;
    MONTH: string;
    DAY: string;
    TIME: string;
    EG_GH_ID: string;
    T: string;
    H: string;
    SRA1H: string;
    P: string;
    F: string;
    D: string;
    SSV1H: string;
    HPU1: string;
    HPU2: string;
    HPU3: string;
}
