export interface IMeasurementDto {
    measured_at: string;
    station_id: string;
    year: number;
    month: number;
    day: number;
    time: string;
    air_temp: number | null;
    air_hum: number | null;
    precip: number | null;
    air_pressure: number | null;
    wind_speed: number | null;
    wind_dir: number | null;
    sunlight: number | null;
    soil_hum_0_10: number | null;
    soil_hum_10_50: number | null;
    soil_hum_50_100: number | null;
}
