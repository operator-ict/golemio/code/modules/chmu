import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IMeasurementDto } from "#sch/definitions/interfaces/IMeasurementDto";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export class MeasurementModel extends Model<IMeasurementDto> implements IMeasurementDto {
    declare measured_at: string;
    declare station_id: string;
    declare year: number;
    declare month: number;
    declare day: number;
    declare time: string;
    declare air_temp: number | null;
    declare air_hum: number | null;
    declare precip: number | null;
    declare air_pressure: number | null;
    declare wind_speed: number | null;
    declare wind_dir: number | null;
    declare sunlight: number | null;
    declare soil_hum_0_10: number | null;
    declare soil_hum_10_50: number | null;
    declare soil_hum_50_100: number | null;

    public static attributeModel: ModelAttributes<MeasurementModel, IMeasurementDto> = {
        measured_at: { type: DataTypes.DATE, primaryKey: true },
        station_id: { type: DataTypes.STRING, primaryKey: true },
        year: { type: DataTypes.INTEGER },
        month: { type: DataTypes.INTEGER },
        day: { type: DataTypes.INTEGER },
        time: { type: DataTypes.TIME },
        air_temp: { type: DataTypes.FLOAT, allowNull: true },
        air_hum: { type: DataTypes.FLOAT, allowNull: true },
        precip: { type: DataTypes.FLOAT, allowNull: true },
        air_pressure: { type: DataTypes.FLOAT, allowNull: true },
        wind_speed: { type: DataTypes.FLOAT, allowNull: true },
        wind_dir: { type: DataTypes.FLOAT, allowNull: true },
        sunlight: { type: DataTypes.FLOAT, allowNull: true },
        soil_hum_0_10: { type: DataTypes.INTEGER, allowNull: true },
        soil_hum_10_50: { type: DataTypes.INTEGER, allowNull: true },
        soil_hum_50_100: { type: DataTypes.INTEGER, allowNull: true },
    };

    public static jsonSchema: JSONSchemaType<IMeasurementDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                measured_at: { type: "string" },
                station_id: { type: "string" },
                year: { type: "integer" },
                month: { type: "integer" },
                day: { type: "integer" },
                time: { type: "string" },
                air_temp: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                air_hum: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                precip: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                air_pressure: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                wind_speed: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                wind_dir: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                sunlight: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                soil_hum_0_10: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                soil_hum_10_50: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                soil_hum_50_100: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
            },
            required: ["measured_at", "station_id"],
        },
    };
}
