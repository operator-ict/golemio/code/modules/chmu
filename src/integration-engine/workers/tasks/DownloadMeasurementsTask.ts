import { MeasurementsRepository } from "#ie/data-access/MeasurementsRepository";
import ChmuDatasource from "#ie/datasources/ChmuDatasource";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { MeasurementsTransformation } from "#ie/transformations/MeasurementsTransformation";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors/QueueManager";
import { AbstractGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IMeasurementDto } from "#sch/definitions/interfaces/IMeasurementDto";
import IDownloadMeasurementsMessage from "../interfaces/IDownloadMeasurementsMessage";
import { DownloadMeasurementsMessageSchema } from "./DownloadMeasurementsMessageSchema";

@injectable()
export default class DownloadMeasurementsTask extends AbstractTask<IDownloadMeasurementsMessage> {
    public queueName: string = "downloadMeasurementsTask";
    protected schema = DownloadMeasurementsMessageSchema;

    constructor(
        @inject(ModuleContainerToken.ChmuDatasource) private datasource: ChmuDatasource,
        @inject(ModuleContainerToken.MeasurementsTransformation) private transformation: MeasurementsTransformation,
        @inject(ModuleContainerToken.MeasurementsRepository) private repository: MeasurementsRepository,
        @inject(ContainerToken.Logger) private logger: ILogger
    ) {
        super(""); // done in worker
    }

    protected execute = async (data: IDownloadMeasurementsMessage): Promise<void> => {
        const { from, to } = this.normalizeInput(data);

        if (from.equals(to)) {
            await this.downloadData(from.toJSDate());
        } else {
            await this.generateSubTasks(from, to);
        }
    };

    private async downloadData(date: Date) {
        try {
            const inputData = await this.datasource.getData(date);
            const transformedData: IMeasurementDto[] = await this.transformation.transform(inputData.data);
            await this.repository.bulkUpdate(transformedData);
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            this.logger.error(error, `Unable to download chmu measurements data.`);
        }
    }

    private generateSubTasks = async (from: DateTime, to: DateTime) => {
        const intervals = this.generateIntervals(from, to);

        for (const interval of intervals) {
            await QueueManager.sendMessageToExchange(this.queuePrefix, this.queueName, {
                from: interval.toISO(),
                to: interval.toISO(),
            });
        }
    };

    private normalizeInput(data: IDownloadMeasurementsMessage) {
        const now = DateTime.now();
        const from = data.from ? DateTime.fromISO(data.from).set({ minute: 0, second: 0, millisecond: 0 }) : now;
        const to = data.from && data.to ? DateTime.fromISO(data.to).set({ minute: 0, second: 0, millisecond: 0 }) : now;

        return { from, to };
    }

    private generateIntervals = (from: DateTime, to: DateTime) => {
        const intervals = [];
        let tmpFrom = from;

        do {
            intervals.push(tmpFrom);
            tmpFrom = tmpFrom.plus({ hour: 1 });
        } while (tmpFrom < to);

        return intervals;
    };
}
