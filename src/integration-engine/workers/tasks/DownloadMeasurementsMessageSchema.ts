import { IsISO8601, IsOptional, IsString } from "@golemio/core/dist/shared/class-validator";
import IDownloadMeasurementsMessage from "../interfaces/IDownloadMeasurementsMessage";

export class DownloadMeasurementsMessageSchema implements IDownloadMeasurementsMessage {
    @IsOptional()
    @IsString()
    @IsISO8601()
    from?: string;
    @IsOptional()
    @IsString()
    @IsISO8601()
    to?: string;
}
