import { ChmuContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { MODULE_NAME } from "src/const";
import DownloadMeasurementsTask from "./tasks/DownloadMeasurementsTask";

export class ChmuWorker extends AbstractWorker {
    protected name = MODULE_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(ChmuContainer.resolve<DownloadMeasurementsTask>(ModuleContainerToken.DownloadMeasurementsTask));
    }

    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
