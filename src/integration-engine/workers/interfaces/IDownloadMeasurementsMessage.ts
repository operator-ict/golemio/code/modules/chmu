export default interface IDownloadMeasurementsMessage {
    from?: string;
    to?: string;
}
