import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { SCHEMA_NAME } from "src/const";
import { IMeasurementDto } from "#sch/definitions/interfaces/IMeasurementDto";
import { MeasurementModel } from "../../schema-definitions/definitions/models/MeasurementModel";

@injectable()
export class MeasurementsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "ChmuMeasurementsRepository",
            {
                outputSequelizeAttributes: MeasurementModel.attributeModel,
                pgTableName: "measurements",
                pgSchema: SCHEMA_NAME,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("ChmuMeasurementsValidator", MeasurementModel.jsonSchema)
        );
    }

    public bulkUpdate = async (data: IMeasurementDto[]) => {
        await this.validate(data);
        await this.sequelizeModel.bulkCreate<MeasurementModel>(data, {
            updateOnDuplicate: this.getUpdateAttributes(),
        });
    };

    private getUpdateAttributes = () => {
        return Object.keys(MeasurementModel.attributeModel)
            .filter((att) => !["vendor_id", "pick_at"].includes(att))
            .concat("updated_at") as Array<keyof IMeasurementDto>;
    };
}
