const ModuleContainerToken = {
    ChmuDatasource: Symbol(),
    MeasurementsRepository: Symbol(),
    MeasurementsTransformation: Symbol(),
    DownloadMeasurementsTask: Symbol(),
};

export { ModuleContainerToken };
