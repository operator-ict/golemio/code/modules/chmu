import { MeasurementsRepository } from "#ie/data-access/MeasurementsRepository";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import ChmuDatasource from "#ie/datasources/ChmuDatasource";
import { MeasurementsTransformation } from "#ie/transformations/MeasurementsTransformation";
import DownloadMeasurementsTask from "#ie/workers/tasks/DownloadMeasurementsTask";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";

//#region Initialization
const ChmuContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Datasources
ChmuContainer.registerSingleton(ModuleContainerToken.ChmuDatasource, ChmuDatasource);
//#endregion

//#region Transformations
ChmuContainer.registerSingleton(ModuleContainerToken.MeasurementsTransformation, MeasurementsTransformation);
//#endregion

//#region Repositories
ChmuContainer.registerSingleton(ModuleContainerToken.MeasurementsRepository, MeasurementsRepository);
//#endregion

//#region WorkerTasks
ChmuContainer.registerSingleton(ModuleContainerToken.DownloadMeasurementsTask, DownloadMeasurementsTask);
//#endregion

export { ChmuContainer };
