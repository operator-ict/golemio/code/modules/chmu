import IInputChmuData from "#sch/datasources/interfaces/IInputChmuData";
import { IMeasurementDto } from "#sch/definitions/interfaces/IMeasurementDto";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class MeasurementsTransformation extends BaseTransformation implements ITransformation {
    public name = "MeasurementsTransformation";
    private isMissingLeadingZeroRegex = /^-?,\d+$/;

    constructor() {
        super();
    }

    protected transformElement = (element: IInputChmuData): IMeasurementDto => {
        const year = Number.parseInt(element.YEAR);
        const month = Number.parseInt(element.MONTH);
        const day = Number.parseInt(element.DAY);
        const hour = Number.parseInt(element.TIME.split(":")[0]);

        return {
            measured_at: DateTime.local(year, month, day, hour, {
                zone: "Europe/Prague",
            }).toISO(),
            station_id: element.EG_GH_ID,
            year: Number.parseInt(element.YEAR),
            month: Number.parseInt(element.MONTH),
            day: Number.parseInt(element.DAY),
            time: element.TIME,
            air_temp: this.transformDecimalValue(element.T),
            air_hum: this.transformDecimalValue(element.H),
            precip: this.transformDecimalValue(element.SRA1H),
            air_pressure: this.transformDecimalValue(element.P),
            wind_speed: this.transformDecimalValue(element.F),
            wind_dir: this.transformDecimalValue(element.D),
            sunlight: this.transformDecimalValue(element.SSV1H),
            soil_hum_0_10: element.HPU1.length > 0 ? Number(element.HPU1) : null,
            soil_hum_10_50: element.HPU2.length > 0 ? Number(element.HPU2) : null,
            soil_hum_50_100: element.HPU3.length > 0 ? Number(element.HPU3) : null,
        };
    };

    private transformDecimalValue = (value: string | null | undefined) => {
        if (!value || value.length === 0) {
            return null;
        }

        const temp = value.replace(",", ".");

        return this.isMissingLeadingZeroRegex.test(temp) ? Number(temp.replace(".", "0.")) : Number(temp);
    };
}
