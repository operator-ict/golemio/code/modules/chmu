import { CSVDataTypeStrategy, DataSource, FTPProtocolStrategy, IFTPSettings } from "@golemio/core/dist/integration-engine";
import { IntegrationEngineConfiguration } from "@golemio/core/dist/integration-engine/config/IntegrationEngineConfiguration";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import ChmuSchemaProvider from "../../schema-definitions/datasources/ChmuSchemaProvider";
import IInputChmuData from "../../schema-definitions/datasources/interfaces/IInputChmuData";

@injectable()
export default class ChmuDatasource {
    public name = "ChmuDatasource";
    private readonly INPUT_ENCODING = "utf8";

    constructor(@inject(ContainerToken.Config) private readonly config: IntegrationEngineConfiguration) {}

    public getData(date: Date = new Date()): Promise<{ data: IInputChmuData[]; filepath: string; name: string }> {
        const result = this.getCurrentDatasource(date).getAll();

        return result;
    }

    private getCurrentDatasource(date: Date) {
        return new DataSource(
            "ChmuFtpDataSource",
            new FTPProtocolStrategy(this.getConfiguration(date)),
            new CSVDataTypeStrategy({
                fastcsvParams: { delimiter: ";", headers: true },
                subscribe: (json: any) => json,
            }),
            new JSONSchemaValidator("ChmuFtpDataSourceValidator", ChmuSchemaProvider.get())
        );
    }

    private getConfiguration(date: Date): IFTPSettings {
        return {
            filename: this.generateFileName(date),
            path: this.config.datasources.chmu.ftpInput.path,
            url: {
                host: this.config.datasources.chmu.ftpInput.host,
                port: this.config.datasources.chmu.ftpInput.port,
                user: this.config.datasources.chmu.ftpInput.user,
                password: this.config.datasources.chmu.ftpInput.password,
            },
            encoding: this.INPUT_ENCODING,
        };
    }

    private generateFileName(date: Date) {
        const [year, month, day, hour] = [
            date.getFullYear(),
            (date.getMonth() + 1).toString().padStart(2, "0"), // to avoid zero based value +1
            date.getDate().toString().padStart(2, "0"),
            date.getHours().toString().padStart(2, "0"),
        ];

        //expected filename 6443_YYYYMMDDHH.txt
        return `6443_${year}${month}${day}${hour}.txt`;
    }
}
